async function deleteShoe(shoe) {
    const shoeUrl = `http://localhost:8080/api/shoes/${shoe.id}/`
    const fetchConfig = {
        method: "delete",
        headers: {
          'Content-Type': 'application/json',
        },
      };
    await fetch(shoeUrl, fetchConfig);
}

function ShoesList({shoes}) {
    if (shoes === undefined) {
        return null;
    }
    return (
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Manufacturer</th>
                <th>Model Name</th>
                <th>Color</th>
                <th>Picture</th>
            </tr>
            </thead>
            <tbody>
            {shoes.map(shoe => {
                console.log(shoe);
                return (
                <tr key={shoe.id}>
                    <td>{ shoe.manufacturer }</td>
                    <td>{ shoe.shoe_name }</td>
                    <td>{ shoe.shoe_color }</td>
                    <td>{ shoe.shoe_photo_url }</td>
                    <td>
                        <img
                            src={shoe.picture}
                            alt=""
                            width="90"
                            height="90"
                            />
                    </td>
                    <td><button type="button" className="btn btn-danger" onClick={() => deleteShoe(shoe)}>Delete</button> </td>
                </tr>
                );
            })}

            </tbody>
        </table>
    );
}




export default ShoesList;
