import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import HatList from './HatList';
import HatForm from './HatForm';
import ShoesList from './ShoesList';
import ShoesForm from './ShoesForm';
import Nav from './Nav';
import { useState, useEffect } from 'react';



function App(props) {

  const [shoes, setShoes] = useState([]);
  const [hats, setHats] = useState([]);

  const getShoes = async () => {
    const shoeUrl = "http://localhost:8080/api/shoes/"
    const response = await fetch(shoeUrl)

    if (response.ok) {
      const data = await response.json()
      const shoes = data.shoes
      setShoes(shoes);
    }
  }
  const getHats = async () => {
    const hatUrl = "http://localhost:8090/api/hats/"
    const response = await fetch(hatUrl)

    if (response.ok) {
      const data = await response.json()
      const hats = data.hats
      setHats(hats);
    }
  }

  useEffect(() => {
    getShoes();
    getHats();
  }, []);

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          {/* Main Page Route */}
          <Route path="/" element={<MainPage />} />

          {/* Hat list and form Routes */}
          <Route path="/hats" element={<HatList hats={hats}/>} />
          <Route path="hats">
            <Route path="new" element={<HatForm />} />
          </Route>

          {/* Shoe list and form Routes */}
          <Route path="/shoes" element={<ShoesList shoes={shoes}/>} />
          <Route path="shoes">
            <Route path="new" element={<ShoesForm /> } />
          </Route>

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
